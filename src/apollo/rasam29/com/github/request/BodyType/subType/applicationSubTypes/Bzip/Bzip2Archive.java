package apollo.rasam29.com.github.request.BodyType.subType.applicationSubTypes.Bzip;


import apollo.rasam29.com.github.request.BodyType.SubType;

/**
 * Created by R.Arabzadeh on 9/1/2018.
 */
public class Bzip2Archive implements SubType {
    @Override
    public String setSubtype() {
        return "x-bzip2";
    }
}
