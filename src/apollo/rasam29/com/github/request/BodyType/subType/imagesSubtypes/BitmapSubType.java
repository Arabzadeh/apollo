package apollo.rasam29.com.github.request.BodyType.subType.imagesSubtypes;

import apollo.rasam29.com.github.request.BodyType.SubType;

/**
 * Created by R.Arabzadeh on 9/1/2018.
 */
public class BitmapSubType implements SubType{
    @Override
    public String setSubtype() {
        return "bmp";
    }
}
