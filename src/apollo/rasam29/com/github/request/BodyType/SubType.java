package apollo.rasam29.com.github.request.BodyType;

/**
 * Created by R.Arabzadeh on 8/27/2018.
 */
public interface SubType {
    String setSubtype();
    default String getSubType(){
        return "/"+setSubtype();
    }
}
