package apollo.rasam29.com.github.request.BodyType.type;


import apollo.rasam29.com.github.request.BodyType.ContentType;
import apollo.rasam29.com.github.request.BodyType.SubType;

/**
 * Created by R.Arabzadeh on 8/27/2018.
 */
public class Application implements ContentType {
    @Override
    public String getContentType(SubType subType) {
        return "application"+subType.getSubType();
    }
}
