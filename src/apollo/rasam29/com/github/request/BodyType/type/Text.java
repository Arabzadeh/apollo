package apollo.rasam29.com.github.request.BodyType.type;

import apollo.rasam29.com.github.request.BodyType.ContentType;
import apollo.rasam29.com.github.request.BodyType.SubType;

public class Text implements ContentType {
    @Override
    public String getContentType(SubType subType) {
        return "text" + subType.getSubType();
    }
}
