package apollo.rasam29.com.github.request.httpMethods;

import apollo.rasam29.com.github.request.ApolloRequest;
import apollo.rasam29.com.github.request.ApolloResponse;

public interface RestFullStrategy<T> {
    ApolloResponse<T> doTheRequest(ApolloRequest apolloRequest);
}
