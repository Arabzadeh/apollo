package apollo.rasam29.com.github.request.httpMethods;


public interface HttpMethod<T> {
    RestFullStrategy<T> getMethod();
}
