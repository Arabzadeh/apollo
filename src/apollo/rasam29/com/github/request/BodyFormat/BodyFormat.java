package apollo.rasam29.com.github.request.BodyFormat;

public interface BodyFormat {
    String getFormat();
}
