package apollo.rasam29.com.github.request;

import apollo.rasam29.com.github.pojos.Header;
import apollo.rasam29.com.github.request.httpMethods.HttpMethod;

public class ApolloRequest {

    private Object t;
    private Header header;
    private HttpMethod httpMethod;
    private Route route;

    private ApolloRequest(Object t, Header header, HttpMethod httpMethod, Route route) {
        this.t = t;
        this.header = header;
        this.httpMethod = httpMethod;
        this.route = route;
    }
    public ApolloResponse excute(){
        return httpMethod.getMethod().doTheRequest(this);
    }

    public class ApolloRequestBuilder{
       private Object t;
       private Header header;
       private HttpMethod httpMethod;
       private Route route;

       public ApolloRequestBuilder setT(Object t) {
           this.t = t;
           return this;
       }

       public ApolloRequestBuilder setHeader(Header header) {
           this.header = header;
           return this;
       }

       public ApolloRequestBuilder setHttpMethod(HttpMethod httpMethod) {
           this.httpMethod = httpMethod;
           return this;
       }

       public ApolloRequestBuilder setRoute(Route route) {
           this.route = route;
           return this;
       }

       public ApolloRequest build(){
           return new ApolloRequest(t,header,httpMethod,route);
       }
   }

}
