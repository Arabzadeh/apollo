package apollo.rasam29.com.github.request;

import apollo.rasam29.com.github.error.exceptions.MallFormedRoutesException;

public class Route {
    private String route;

    public Route(String route) throws Exception {
        if (!validateRoute(route))
            throw new MallFormedRoutesException();
        this.route = route;
    }

    private boolean validateRoute(String route){
        return true;
    }
}
