package apollo.rasam29.com.github.request;

import apollo.rasam29.com.github.pojos.Header;
import apollo.rasam29.com.github.request.BodyType.ContentType;

public class ApolloResponse<T> {

    private final Header header;
    private T model;
    private int statusCode;
    private Throwable throwable;


    private ApolloResponse(T model, int statusCode, Throwable throwable,Header header) {
        this.model = model;
        this.statusCode = statusCode;
        this.throwable = throwable;
        this.header = header;
    }



    private Header header(){
        return header;
    }


    public T getModel() {
        return model;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public Throwable getThrowable() {
        return throwable;
    }


    public class ResponseBuilder<T>{

        private T model;
        private int statusCode;
        private Throwable throwable;
        private Header headers;

        public ResponseBuilder data(T data){
            model = data;
            return this;
        }
        public ResponseBuilder code(int code) {
            statusCode = code;
            return this;
        }

        public ResponseBuilder error(Throwable error) {
            throwable = error;
            return this;
        }

        public ResponseBuilder header(Header header) {
            headers = header;
            return this;
        }


        public ApolloResponse<T> build(){
            return new ApolloResponse<>(model,statusCode,throwable,header);
        }

    }
}
