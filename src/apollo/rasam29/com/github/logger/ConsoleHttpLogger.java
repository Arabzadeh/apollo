package apollo.rasam29.com.github.logger;

import apollo.rasam29.com.github.pojos.Header;
import apollo.rasam29.com.github.request.ApolloRequest;
import apollo.rasam29.com.github.request.ApolloResponse;
import apollo.rasam29.com.github.request.httpMethods.HttpMethod;

/**
 * Created by R.Arabzadeh Taktell on 11/21/2018.
 */
public class ConsoleHttpLogger implements HttpLogger {
    @Override
    public void handleUrl(String url) {
        System.out.println(url);
    }

    @Override
    public void handleHeaders(Header headers) {

    }

    @Override
    public void handleMethod(HttpMethod httpMethod) {

    }

    @Override
    public void handleResponse(ApolloResponse apolloResponse) {

    }

    @Override
    public void handleRequestBody(ApolloRequest apolloRequest) {

    }
}
