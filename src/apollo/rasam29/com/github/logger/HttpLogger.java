package apollo.rasam29.com.github.logger;

import apollo.rasam29.com.github.pojos.Header;
import apollo.rasam29.com.github.request.ApolloRequest;
import apollo.rasam29.com.github.request.ApolloResponse;
import apollo.rasam29.com.github.request.httpMethods.HttpMethod;

public interface HttpLogger {
    void handleUrl(String url);

    void handleHeaders(Header headers);

    void handleMethod(HttpMethod httpMethod);

    void handleResponse(ApolloResponse apolloResponse);

    void handleRequestBody(ApolloRequest apolloRequest);

}
