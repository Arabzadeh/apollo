package apollo.rasam29.com.github.ApolloEngine.retryStrategy;

import apollo.rasam29.com.github.ApolloEngine.retryStrategy.debounnceFormulas.DebounceFormula;
import apollo.rasam29.com.github.request.ApolloRequest;
import apollo.rasam29.com.github.request.ApolloResponse;

/**
 * Created by R.Arabzadeh on 11/21/2018.
 */
public interface RetryStrategy {
    void retry(ApolloRequest apolloRequest);

    boolean getStrategy(ApolloResponse ApolloResponse);

    int timeOut();

    boolean wantDebounce();

    int numberOfAttempts();

    DebounceFormula getDebounceFormula();

    default void retry(){
        
    }
}
