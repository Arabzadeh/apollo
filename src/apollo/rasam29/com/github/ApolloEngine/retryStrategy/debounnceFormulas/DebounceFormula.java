package apollo.rasam29.com.github.ApolloEngine.retryStrategy.debounnceFormulas;

/**
 * Created by R.Arabzadeh Taktell on 11/21/2018.
 */
public interface DebounceFormula {
    int nextsleepTime(int currentSleepTime);
}
