package apollo.rasam29.com.github.ApolloEngine.retryStrategy;

import apollo.rasam29.com.github.ApolloEngine.retryStrategy.debounnceFormulas.DebounceFormula;
import apollo.rasam29.com.github.request.ApolloRequest;
import apollo.rasam29.com.github.request.ApolloResponse;

/**
 * Created by R.Arabzadeh Taktell on 11/21/2018.
 */
public class DefaultStrategy implements RetryStrategy {

    public static final int TIME_OUT = 3000;
    public static final int READ_TIME_OUT = 3000;
    public static final int INFINITE_ATTEMPTS = -1;
    public static final int DEFAULT_ATTEMPTS = 3;


    @Override
    public void retry(ApolloRequest apolloRequest) {
        apolloRequest.excute();
    }

    @Override
    public boolean getStrategy(ApolloResponse apolloResponse) {
        return apolloResponse.getThrowable()!= null;
    }

    @Override
    public int timeOut() {
        return TIME_OUT;
    }

    @Override
    public boolean wantDebounce() {
        return true;
    }

    @Override
    public int numberOfAttempts() {
        return DEFAULT_ATTEMPTS;
    }

    @Override
    public DebounceFormula getDebounceFormula() {
        return null;
    }
}
