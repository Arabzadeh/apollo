package apollo.rasam29.com.github.ApolloEngine;

import apollo.rasam29.com.github.ApolloEngine.retryStrategy.RetryStrategy;
import apollo.rasam29.com.github.logger.HttpLogger;


import java.util.concurrent.TimeUnit;

public final class ApolloEngine implements BaseEngine {

    private String baseUrl;
    private HttpLogger httpLogger;
    private RetryStrategy retryStrategy;
    private int timeOut;
    private int readTimeOut;
    private TimeUnit timeUnit;

    private ApolloEngine(String baseUrl, HttpLogger httpLogger, RetryStrategy retryStrategy, int timeOut, int readTimeOut, TimeUnit timeUnit) {
        this.baseUrl = baseUrl;
        this.httpLogger = httpLogger;
        this.retryStrategy = retryStrategy;
        this.timeOut = timeOut;
        this.readTimeOut = readTimeOut;
        this.timeUnit = timeUnit;
    }

    @Override
    public String getBaseUrl() {
        return baseUrl;
    }

    @Override
    public HttpLogger getHttpLogger() {
        return httpLogger;
    }

    @Override
    public RetryStrategy getRetryStrategy() {
        return retryStrategy;
    }

    @Override
    public int getTimeOut() {
        return timeOut;
    }

    @Override
    public int getReadTimeOut() {
        return readTimeOut;
    }

    @Override
    public TimeUnit getTimeUnit() {
        return timeUnit;
    }


    public class ApolloEngineBuilder {

        private String baseUrl;
        private HttpLogger httpLogger;
        private RetryStrategy retryStrategy;
        private int timeOut;
        private int readTimeOut;
        private TimeUnit timeUnit;

        public ApolloEngineBuilder baseUrl(String baseUrl) {
            this.baseUrl = baseUrl;
            return this;
        }

        public ApolloEngineBuilder setHttpLogger(HttpLogger httpLogger) {
            this.httpLogger = httpLogger;
            return this;
        }

        public ApolloEngineBuilder retryStrategy(RetryStrategy retryStrategy) {
            this.retryStrategy = retryStrategy;
            return this;
        }

        public ApolloEngineBuilder timeOut(int timout) {
            this.timeOut = timout;
            return this;
        }

        public ApolloEngineBuilder readTimeout(int readTimeOut) {
            this.readTimeOut = readTimeOut;
            return this;
        }

        public ApolloEngineBuilder timeUnit(TimeUnit timeUnit){
            this.timeUnit = timeUnit;
            return this;
        }

        public ApolloEngine build() {
            return new ApolloEngine(baseUrl,httpLogger,retryStrategy,timeOut,readTimeOut,timeUnit);
        }
    }
}
