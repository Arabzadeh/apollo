package apollo.rasam29.com.github.ApolloEngine;

import apollo.rasam29.com.github.ApolloEngine.retryStrategy.RetryStrategy;
import apollo.rasam29.com.github.logger.HttpLogger;

import java.util.concurrent.TimeUnit;

public interface BaseEngine {

    String getBaseUrl();

    HttpLogger getHttpLogger();

    RetryStrategy getRetryStrategy();

    int getTimeOut();

    int getReadTimeOut();

    TimeUnit getTimeUnit();

}
